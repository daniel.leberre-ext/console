<?php
/**
 * CONSOLE FORGE EDU
 *
 * Description : Console de la ForgeEdu.
 *
 * @author     Laurent Abbal <laurent@abbal.com>
 * @license    https://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 * @version    1.0.0
 * @link       https://forge.apps.education.fr/docs/console
 *
 * Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
 * selon les termes de la Licence Publique Générale GNU publiée par la Free Software Foundation,
 * soit la version 3 de la Licence, soit (à votre choix) toute version ultérieure.
 *
 * Ce programme est distribué dans l'espoir qu'il sera utile,
 * mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
 * QUALITÉ MARCHANDE ou d'ADÉQUATION À UN USAGE PARTICULIER.
 * Voir la Licence Publique Générale GNU pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
 * avec ce programme. Si ce n'est pas le cas, consultez <https://www.gnu.org/licenses/>.
 */


// Désactiver les rapports d'erreurs
error_reporting(0);

define('PRIVATE_TOKEN', getenv('PRIVATE_TOKEN'));
define('NB_PROJETS', 18);
define('NB_JOURS', 10);
define('GITLAB_BASE_URL','https://forge.apps.education.fr/');

function splitAndFilterText($text) {
    // Utiliser une expression régulière pour découper le texte en blocs
    $blocks = preg_split('/\n*###\n*/', $text);

    // Filtrer les blocs pour enlever ceux qui commencent par '###'
    $filteredBlocks = array_filter($blocks, function($block) {
        return !preg_match('/^###/', trim($block));
    });

    return $filteredBlocks;
}

function convertMarkdownToHtml($markdownText) {
    $apiUrl = GITLAB_BASE_URL . 'api/v4/markdown';

    // Préparer les données pour la requête POST
    $data = [
        'text' => $markdownText,
        'gfm' => true
    ];

    // Initialiser cURL
    $ch = curl_init();

    // Configurer les options cURL
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json',
        "Authorization: Bearer " . PRIVATE_TOKEN
    ]);

    // Exécuter la requête et récupérer la réponse
    $response = curl_exec($ch);

    // Vérifier les erreurs
    if (curl_errno($ch)) {
        curl_close($ch);
        return 'Erreur cURL : ' . curl_error($ch);
    }

    // Fermer cURL
    curl_close($ch);

    // Décoder la réponse JSON
    $decodedResponse = json_decode($response, true);

    if (isset($decodedResponse['html'])) {
        // Récupérer le contenu HTML
        return $decodedResponse['html'];
    } else {
        return 'Erreur: la clé "html" est manquante dans la réponse.';
    }
}

function removeFirstLine($text) {
    $lines = explode("\n", $text);
    array_shift($lines);
    $newText = implode("\n", $lines);
    return $newText;
}

function recuperer_forge_stats() {
    $url = GITLAB_BASE_URL . 'api/v4/application/statistics';
    $headers = ['Private-Token: ' . PRIVATE_TOKEN];
    $response = file_get_contents($url, false, stream_context_create(['http' => ['header' => $headers]]));
    if ($response !== false) {
        return json_decode($response, true);
    } else {
        //echo "Erreur lors de la récupération.";
    }
}


function recuperer_projets() {
    $url_base = GITLAB_BASE_URL . 'api/v4/projects';
    $headers = ['Private-Token: ' . PRIVATE_TOKEN];
    $resultat = [];
    $page = 1;
    while (true) {
        $params = [
            //'updated_after' => date('Y-m-d\TH:i:s', strtotime("-$depuis_nb_jours days")),
            //'order_by' => 'updated_at',
            'visibility' => 'public',
            'statistics' => true,
            'license' => true,
            'per_page' => 100,
            'page' => $page
        ];
        $response = file_get_contents($url_base . '?' . http_build_query($params), false, stream_context_create(['http' => ['header' => $headers]]));
        if ($response !== false) {
            $response_page = json_decode($response, true);
            if (empty($response_page)) {
                break;
            }
            $resultat_page = array_map(function ($response_page) {
                return [
                    'id'                    => $response_page['id'],
                    'description'           => $response_page['description'],
                    'name'                  => $response_page['name'],
                    'name_with_namespace'   => $response_page['name_with_namespace'],
                    'path'                  => $response_page['path'],
                    'path_with_namespace'   => $response_page['path_with_namespace'],
                    'created_at'            => $response_page['created_at'],
                    'updated_at'            => $response_page['updated_at'],
                    'web_url'               => $response_page['web_url'],
                    'readme_url'            => $response_page['readme_url'],
                    'forks_count'           => $response_page['forks_count'],
                    'avatar_url'            => $response_page['avatar_url'],
                    'star_count'            => $response_page['star_count'],
                    'commit_count'          => $response_page['statistics']['commit_count'],
                    'visibility'            => $response_page['visibility'],
                    'nb_membres'            => recuperer_projet_nb_membres($response_page['id'])
                ];
            }, $response_page);
            $resultat = array_merge($resultat, $resultat_page);
            $page++;
        } else {
            //echo "Erreur lors de la récupération.";
            break;
        }
    }
    return $resultat;
}


function recuperer_projet_nb_commits($project_id, $depuis_nb_jours) {
    $url_base = GITLAB_BASE_URL . 'api/v4/projects/$project_id/repository/commits';
    $headers = ['Private-Token: ' . PRIVATE_TOKEN];
    $resultat = [];
    $page = 1;
    while (true) {
        $params = [
            'per_page' => 100,
            'page' => $page,
            'since' => date('Y-m-d\TH:i:s', strtotime("-$depuis_nb_jours days")),
            //'all' => true, // tous les commits de toutes les branches
        ];
        $response = file_get_contents($url_base . '?' . http_build_query($params), false, stream_context_create(['http' => ['header' => $headers]]));
        if ($response !== false) {
            $response_page = json_decode($response, true);
            if (empty($response_page)) {
                break;
            }
            $resultat = array_merge($resultat, $response_page);
            $page++;
        } else {
            //echo "Erreur lors de la récupération.";
            break;
        }
    }
    return count($resultat);
}


function recuperer_projet_licence($project_id) {
    $url_base = GITLAB_BASE_URL . "api/v4/projects/$project_id";
    $headers = ['Private-Token: ' . PRIVATE_TOKEN];
    $params = [
        'license' => true
    ];
    $response = file_get_contents($url_base . '?' . http_build_query($params), false, stream_context_create(['http' => ['header' => $headers]]));
    if ($response !== false) {
        $response = json_decode($response, true);
    } else {
        //echo "Erreur lors de la récupération.";
    }
    return [$response['license_url'], $response['license']];
}

function recuperer_projet_nb_membres($project_id) {
    $url_base = GITLAB_BASE_URL . "api/v4/projects/$project_id/members/all";
    $headers = ['Private-Token: ' . PRIVATE_TOKEN];
    $resultat = [];
    $page = 1;
    while (true) {
        $params = [
            'per_page' => 100,
            'page' => $page,
        ];
        $response = file_get_contents($url_base . '?' . http_build_query($params), false, stream_context_create(['http' => ['header' => $headers]]));
        if ($response !== false) {
            $response_page = json_decode($response, true);
            if (empty($response_page)) {
                break;
            }
            $resultat = array_merge($resultat, $response_page);
            $page++;
        } else {
            //echo "Erreur lors de la récupération.";
            break;
        }
    }
    return count($resultat);
}


$forge_stats = recuperer_forge_stats();
$projets = recuperer_projets();
$nb_commits = array_sum(array_column($projets, 'commit_count'));

// on garde que les projets actifs sur les x derniers jours
$projets_actifs = array_filter($projets, function($projet) {
    $date_actuelle = new DateTime();
    $date_updated_at = new DateTime($projet['updated_at']);
    $difference_jours = $date_actuelle->diff($date_updated_at)->days;
    return $difference_jours < NB_JOURS;
});

foreach($projets_actifs AS $key => $projet_actif) {
    $projets_actifs[$key]['recent_commits'] = recuperer_projet_nb_commits($projet_actif['id'], NB_JOURS);
}


// LES X PROJETS ACTIFS
array_multisort(array_column($projets_actifs, 'recent_commits'), SORT_DESC, $projets_actifs);
$projets_actifs = array_slice($projets_actifs, 0, NB_PROJETS);
foreach($projets_actifs AS $key => $projet) {
    $licence = recuperer_projet_licence($projet['id']);
    $projets_actifs[$key]['license_url'] = $licence[0];
    $projets_actifs[$key]['license'] = $licence[1];
}


// LES X PROJETS STARRED
array_multisort(array_column($projets, 'star_count'), SORT_DESC, $projets);
$projets_starred = array_slice($projets, 0, NB_PROJETS);
foreach($projets_starred AS $key => $projet) {
    $licence = recuperer_projet_licence($projet['id']);
    $projets_starred[$key]['license_url'] = $licence[0];
    $projets_starred[$key]['license'] = $licence[1];
}


// LES X PROJETS FORKED
array_multisort(array_column($projets, 'forks_count'), SORT_DESC, $projets);
$projets_forked = array_slice($projets, 0, NB_PROJETS);
foreach($projets_forked AS $key => $projet) {
    $licence = recuperer_projet_licence($projet['id']);
    $projets_forked[$key]['license_url'] = $licence[0];
    $projets_forked[$key]['license'] = $licence[1];
}

// LES X AVEC LE + DE MEMBRES
array_multisort(array_column($projets, 'nb_membres'), SORT_DESC, $projets);
$projets_nb_membres = array_slice($projets, 0, NB_PROJETS);
foreach($projets_nb_membres AS $key => $projet) {
    $licence = recuperer_projet_licence($projet['id']);
    $projets_nb_membres[$key]['license_url'] = $licence[0];
    $projets_nb_membres[$key]['license'] = $licence[1];
}


//include('data.php');
//$nb_commits = 104837;

/*
echo '<pre>';
print_r(var_export($forge_stats));
print_r(var_export($projets_actifs));
print_r(var_export($projets_starred));
print_r(var_export($projets_forked));
echo '</pre>';
exit;
*/


$page = file_get_contents('https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/index.md');
$blocs = explode("###", $page);
$bloc_1 = removeFirstLine(convertMarkdownToHtml($blocs[count($blocs) - 2]));
$bloc_2 = removeFirstLine(convertMarkdownToHtml($blocs[count($blocs) - 1]));


$metriques = [
    ['&nbsp;', 'COMPTES', str_replace(",", "", $forge_stats['users'])],
    ['&nbsp;', 'GROUPES', str_replace(",", "", $forge_stats['groups'])],
    ['&nbsp;', 'PROJETS', str_replace(",", "", $forge_stats['projects'])],
    ['"issues"', 'TICKETS', str_replace(",", "", $forge_stats['issues'])],
    ['"merge requests"', 'REQUÊTES DE FUSION', str_replace(",", "", $forge_stats['merge_requests'])],
    ['"forks"', 'BIFURCATIONS', str_replace(",", "", $forge_stats['forks'])],
    ['"commits"', 'VALIDATIONS', str_replace(",", "", $nb_commits)],
]

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://docs.forge.apps.education.fr/assets/images/favicon.png">
    <title>CONSOLE DE LA FORGE</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

    <style>
        table {
            border-collapse: separate !important;
            border-spacing: 0 5px !important;
        }

        tr td:first-child {
            border-top-left-radius: 4px !important;
            border-bottom-left-radius: 4px !important;
        }

        tr td:last-child {
            border-top-right-radius: 4px !important;
            border-bottom-right-radius: 4px !important;
        }

        tr:hover td {
            background-color: #f8f8ff !important;
            cursor: pointer;
        }

        td {
            padding: 10px !important;
            background-color: #f8fafc !important;
            border: solid 0px #000 !important;
        }

        a {
            text-decoration: none ;
        }

        a:hover {
            text-decoration: underline;
        }

        td img {
            border-radius: 50%;
            width: 32px;
        }

        .metrique_valeur {
            font-size:4.8rem;
            color:#33539d;
            line-height:4rem;
            margin-bottom:3rem;
        }

        .description:hover path {
            fill: #5c636a;
        }

    </style>

  </head>

<body>

    <div class="container">

        <div class="text-center">
            <img class="img-fluid" src="https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/logo_forge_large.svg?ref_type=heads" width="500" />
            <br /><span class="font-monospace text-secondary">~ CONSOLE ~</span>
            <br /><span class="font-monospace" style="color:silver;opactiy:0.5;font-size:70%">maj <?php echo date("d-m-Y") ?></span>
        </div>

        <div class="row mt-5">
            <?php
            foreach ($metriques AS $metrique) {
                echo "
                <div class='col ps-4 pe-4'>
                    <div class='ps-3 text-muted text-italic small font-monospace' style='font-size:60%;color:silver;line-height:50%;'>$metrique[0]</div>
                    <div class='ps-3 text-nowrap fw-bold text-secondary'>$metrique[1]</div>
                    <div class='fw-bold font-monospace text-nowrap metrique_valeur'>$metrique[2]</div>
                </div>                
                ";
            }
            ?>
        </div>
        
    </div>


    <?php
    /*
    <div class="container mt-4">
        <div class="row">
            <div class="col">
                <div class="fw-bold font-monospace fs-3 lh-1">Logiciels libres</div>
             
                <!-- CARDS -->
                <div class="mt-2 row row-cols-1 row-cols-md-1 g-2 font-monospace small">

                    <div class='col'>
                        <div class='card h-100' style='background-color:#f1f3f5;'>
                            <div class='card-body p-2'>
                                <?php echo $bloc_1 ?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /CARDS -->

            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="fw-bold font-monospace fs-3 lh-1">Ressources éducatives libres</div>
             
                <!-- CARDS -->
                <div class="mt-2 row row-cols-1 row-cols-md-1 g-2 font-monospace small">

                    <div class='col'>
                        <div class='card h-100' style='background-color:#f1f3f5;'>
                            <div class='card-body p-2'>
                                <?php echo $bloc_2 ?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /CARDS -->

            </div>
        </div>
    </div>   
    */
    ?>


    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="fw-bold font-monospace fs-3 lh-1">PROJETS LES PLUS ACTIFS sur les 10 derniers jours</div>
                <div class="fw-bold font-monospace text-muted small lh-1">en nombre de validations ("commits")</div>
             
                <?php
                /*
                <!-- TABLE -->
                <table class="table table-borderless table-sm font-monospace small mt-2">
                <?php
                // lignes
                foreach ($projets_actifs as $projet) {
                    echo "
                    <tr>

                        <td style='vertical-align: top;'>
                            ";
                            if ($projet['avatar_url']) {
                                echo "<img class='m-1' style='border-radius: 50%;' src='" . $projet['avatar_url'] . "' width='24' />";
                            } else {
                                echo "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='24' />";
                            } 
                            echo "
                        </td>

                        <td style='width:100%'>
                            <a href='" . $projet['web_url'] . "' target='_blank'>" . $projet['name'] . "</a>";
                            if ($projet['description']) echo "<div class='text-muted'>" . $projet['description'] . "</div>"; 
                            echo "
                        </td>

                        <td style='text-align:center;vertical-align:middle;'>
                            <svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M8 10.5a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5ZM8 12c1.953 0 3.579-1.4 3.93-3.25h3.32a.75.75 0 0 0 0-1.5h-3.32a4.001 4.001 0 0 0-7.86 0H.75a.75.75 0 0 0 0 1.5h3.32A4.001 4.001 0 0 0 8 12Z' fill='#000'/></svg><br />".$projet['recent_commits']."
                        </td>

                        <td>";
                            if ($projet['readme_url']) echo "<div><a role='button' href='".$projet['readme_url']."' class='btn btn-secondary' style='--bs-btn-padding-y: .01rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .7rem;border-radius:3px;margin-bottom:1px;' target='_blank'>readme</a><div>";
                            if ($projet['license_url']) echo "<div><a role='button' href='".$projet['license_url']."' class='btn btn-secondary' style='--bs-btn-padding-y: .01rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .7rem;border-radius:3px;' target='_blank'>licence</a><div>";
                            echo "
                        </td>                        

                    </tr>";
                }
                ?>
                </table>
                */
                ?>

                <!-- CARDS -->
                <div class="mt-2 row row-cols-1 row-cols-md-6 g-2 font-monospace small">
                <?php
                foreach ($projets_actifs as $projet) {
                    if ($projet['avatar_url']) {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='" . $projet['avatar_url'] . "' width='32' />";
                    } else {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='32' />";
                    } 
                    if ($projet['description']) {
                        $description = $projet['description'];
                    } else {
                        $description = "Pas de description...";
                    }
                    if ($projet['readme_url']) {
                        $readme = "<a href='".$projet['readme_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='README'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M12.5 6v8.5h-9v-13H8v2.75C8 5.216 8.784 6 9.75 6h2.75Zm-.121-1.5L9.5 1.621V4.25c0 .138.112.25.25.25h2.629ZM2 1a1 1 0 0 1 1-1h6.586a1 1 0 0 1 .707.293l3.414 3.414a1 1 0 0 1 .293.707V15a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V1Zm3.75 7a.75.75 0 0 0 0 1.5h4.5a.75.75 0 0 0 0-1.5h-4.5ZM5 11.25a.75.75 0 0 1 .75-.75h2.5a.75.75 0 0 1 0 1.5h-2.5a.75.75 0 0 1-.75-.75Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $readme = "";
                    }
                    if ($projet['license_url']) {
                        $licence = "<a href='".$projet['license_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='".$projet['license']['name']."'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M8 10.5a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9ZM14 6a5.997 5.997 0 0 1-2.886 5.13l.58 3.185L12 16l-1.623-.544L8 14.66l-2.377.796L4 16l.306-1.684.58-3.187A6 6 0 1 1 14 6Zm-7.748 6h3.496l.322 1.772-1.594-.534-.476-.16-.476.16-1.594.534L6.252 12ZM9.5 6a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0ZM11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $licence = "";
                    }                    
                    echo "
                    <div class='col'>
                        <div class='card h-100' style='background-color:#f1f3f5;'>
                            <div class='card-body p-2'>
                                <div style='float:left;padding-right:10px'>".$img."</div>
                                <a href='" . $projet['web_url'] . "' target='_blank'>" . $projet['name'] . "</a>
                            </div>
                            <div class='card-footer text-center ps-1 pe-1' style='border:none;padding-top:0;color:#6c757d;background-color:#f1f3f5;'>

                                <span data-bs-toggle='tooltip' style='cursor:help;' data-bs-html='true' data-bs-title='".htmlentities($description)."'><svg  class='description' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM4.927 4.99c-.285.429-.427.853-.427 1.27 0 .203.09.392.27.566.18.174.4.26.661.26.443 0 .744-.248.903-.746.168-.475.373-.835.616-1.08.243-.244.62-.366 1.134-.366.439 0 .797.12 1.075.363.277.242.416.54.416.892a.97.97 0 0 1-.136.502 1.91 1.91 0 0 1-.336.419 14.35 14.35 0 0 1-.648.558c-.34.282-.611.525-.812.73-.2.205-.362.443-.483.713-.322 1.245 1.35 1.345 1.736.456.047-.086.118-.18.213-.284.096-.103.223-.223.382-.36a41.14 41.14 0 0 0 1.194-1.034c.221-.204.412-.448.573-.73a1.95 1.95 0 0 0 .242-.984c0-.475-.141-.915-.424-1.32-.282-.406-.682-.726-1.2-.962-.518-.235-1.115-.353-1.792-.353-.728 0-1.365.14-1.911.423-.546.282-.961.637-1.246 1.066Zm2.14 7.08a1 1 0 1 0 2 0 1 1 0 0 0-2 0Z' fill='#6c757d'/></svg></span>

                                ".$readme."

                                ".$licence."

                                <svg class='ms-2' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M8 10.5a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5ZM8 12c1.953 0 3.579-1.4 3.93-3.25h3.32a.75.75 0 0 0 0-1.5h-3.32a4.001 4.001 0 0 0-7.86 0H.75a.75.75 0 0 0 0 1.5h3.32A4.001 4.001 0 0 0 8 12Z' fill='#6c757d'/></svg><span class='ms-1' style='font-size:80%'>".$projet['recent_commits']."</span>                                 

                            </div>
                        </div>
                    </div>";
                }
                ?>
                </div>
                <!-- /CARDS -->

            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="fw-bold font-monospace fs-3 lh-1">PROJETS AVEC LE + DE MEMBRES</div>
             
                <!-- CARDS -->
                <div class="mt-2 row row-cols-1 row-cols-md-6 g-2 font-monospace small">
                <?php
                foreach ($projets_nb_membres as $projet) {
                    if ($projet['avatar_url']) {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='" . $projet['avatar_url'] . "' width='32' />";
                    } else {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='32' />";
                    } 
                    if ($projet['description']) {
                        $description = $projet['description'];
                    } else {
                        $description = "Pas de description...";
                    }
                    if ($projet['readme_url']) {
                        $readme = "<a href='".$projet['readme_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='README'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M12.5 6v8.5h-9v-13H8v2.75C8 5.216 8.784 6 9.75 6h2.75Zm-.121-1.5L9.5 1.621V4.25c0 .138.112.25.25.25h2.629ZM2 1a1 1 0 0 1 1-1h6.586a1 1 0 0 1 .707.293l3.414 3.414a1 1 0 0 1 .293.707V15a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V1Zm3.75 7a.75.75 0 0 0 0 1.5h4.5a.75.75 0 0 0 0-1.5h-4.5ZM5 11.25a.75.75 0 0 1 .75-.75h2.5a.75.75 0 0 1 0 1.5h-2.5a.75.75 0 0 1-.75-.75Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $readme = "";
                    }
                    if ($projet['license_url']) {
                        $licence = "<a href='".$projet['license_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='".$projet['license']['name']."'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M8 10.5a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9ZM14 6a5.997 5.997 0 0 1-2.886 5.13l.58 3.185L12 16l-1.623-.544L8 14.66l-2.377.796L4 16l.306-1.684.58-3.187A6 6 0 1 1 14 6Zm-7.748 6h3.496l.322 1.772-1.594-.534-.476-.16-.476.16-1.594.534L6.252 12ZM9.5 6a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0ZM11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $licence = "";
                    }                    
                    echo "
                    <div class='col'>
                        <div class='card h-100' style='background-color:#f1f3f5;'>
                            <div class='card-body p-2'>
                                <div style='float:left;padding-right:10px'>".$img."</div>
                                <a href='" . $projet['web_url'] . "' target='_blank'>" . $projet['name'] . "</a>
                            </div>
                            <div class='card-footer text-center ps-1 pe-1' style='border:none;padding-top:0;color:#6c757d;background-color:#f1f3f5;'>

                                <span data-bs-toggle='tooltip' style='cursor:help;' data-bs-html='true' data-bs-title='".htmlentities($description)."'><svg  class='description' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM4.927 4.99c-.285.429-.427.853-.427 1.27 0 .203.09.392.27.566.18.174.4.26.661.26.443 0 .744-.248.903-.746.168-.475.373-.835.616-1.08.243-.244.62-.366 1.134-.366.439 0 .797.12 1.075.363.277.242.416.54.416.892a.97.97 0 0 1-.136.502 1.91 1.91 0 0 1-.336.419 14.35 14.35 0 0 1-.648.558c-.34.282-.611.525-.812.73-.2.205-.362.443-.483.713-.322 1.245 1.35 1.345 1.736.456.047-.086.118-.18.213-.284.096-.103.223-.223.382-.36a41.14 41.14 0 0 0 1.194-1.034c.221-.204.412-.448.573-.73a1.95 1.95 0 0 0 .242-.984c0-.475-.141-.915-.424-1.32-.282-.406-.682-.726-1.2-.962-.518-.235-1.115-.353-1.792-.353-.728 0-1.365.14-1.911.423-.546.282-.961.637-1.246 1.066Zm2.14 7.08a1 1 0 1 0 2 0 1 1 0 0 0-2 0Z' fill='#6c757d'/></svg></span>

                                ".$readme."

                                ".$licence."

                                <a href='" . $projet['web_url'] . "/-/project_members' target='_blank'>
                                    <svg class='ms-2' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M6.5 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0Zm.63 2.113a3 3 0 1 0-4.259 0A3.997 3.997 0 0 0 1 9.5V13a2 2 0 0 0 2 2h4c.597 0 1.134-.262 1.5-.677.366.415.903.677 1.5.677h3a2 2 0 0 0 2-2v-2c0-1.218-.622-2.29-1.565-2.917a2.5 2.5 0 1 0-3.87 0c-.241.16-.462.35-.656.564a4.005 4.005 0 0 0-1.78-2.534ZM5 7a2.5 2.5 0 0 0-2.5 2.5V13a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5V9.5A2.5 2.5 0 0 0 5 7Zm7.5-.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm-1 2.5a2 2 0 0 0-2 2v2a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-2a2 2 0 0 0-2-2Z' fill='#6c757d'/></svg><span class='ms-1' style='font-size:80%'>".$projet['nb_membres']."</span>                                 
                                </a>
                            </div>
                        </div>
                    </div>";
                }
                ?>
                </div>
                <!-- /CARDS -->

            </div>
        </div>
    </div>    

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="fw-bold font-monospace fs-3 lh-1">PROJETS LES PLUS SUIVIS</div>
                <div class="fw-bold font-monospace text-muted small lh-1">en nombre de favoris ("stars")</div>

                <?php
                /*
                <table class="table table-borderless table-sm font-monospace small mt-2">
                <?php
                foreach ($projets_starred as $projet) {
                    echo "
                    <tr>

                        <td style='vertical-align: top;'>
                            ";
                            if ($projet['avatar_url']) {
                                echo "<img class='m-1' style='border-radius: 50%;' src='" . $projet['avatar_url'] . "' width='24' />";
                            } else {
                                echo "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='24' />";
                            } 
                            echo "
                        </td>

                        <td style='width:100%'>
                            <a href='" . $projet['web_url'] . "' target='_blank'>" . $projet['name'] . "</a>";
                            if ($projet['description']) echo "<div class='text-muted'>" . $projet['description'] . "</div>"; 
                            echo "
                        </td>

                        <td style='text-align:center;vertical-align:middle;'>
                            <svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path d='M7.454 1.694a.591.591 0 0 1 1.092 0l1.585 3.81a.25.25 0 0 0 .21.154l4.114.33a.591.591 0 0 1 .338 1.038L11.658 9.71a.25.25 0 0 0-.08.247l.957 4.015a.591.591 0 0 1-.883.641l-3.522-2.15a.25.25 0 0 0-.26 0l-3.522 2.15a.591.591 0 0 1-.883-.641l.957-4.015a.25.25 0 0 0-.08-.247L1.207 7.026a.591.591 0 0 1 .338-1.038l4.113-.33a.25.25 0 0 0 .211-.153l1.585-3.81Z' fill='#000'/></svg><br />".$projet['star_count']."
                        </td>

                        <td>";
                            if ($projet['readme_url']) echo "<div><a role='button' href='".$projet['readme_url']."' class='btn btn-secondary' style='--bs-btn-padding-y: .01rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .7rem;border-radius:3px;margin-bottom:1px;' target='_blank'>readme</a><div>";
                            if ($projet['license_url']) echo "<div><a role='button' href='".$projet['license_url']."' class='btn btn-secondary' style='--bs-btn-padding-y: .01rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .7rem;border-radius:3px;' target='_blank'>licence</a><div>";
                            echo "
                        </td>                        

                    </tr>";
                }
                ?>
                </table>
                */?>

                <!-- CARDS -->
                <div class="mt-2 row row-cols-1 row-cols-md-6 g-2 font-monospace small">
                <?php
                foreach ($projets_starred as $projet) {
                    if ($projet['avatar_url']) {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='" . $projet['avatar_url'] . "' width='32' />";
                    } else {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='32' />";
                    } 
                    if ($projet['description']) {
                        $description = $projet['description'];
                    } else {
                        $description = "Pas de description...";
                    }
                    if ($projet['readme_url']) {
                        $readme = "<a href='".$projet['readme_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='README'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M12.5 6v8.5h-9v-13H8v2.75C8 5.216 8.784 6 9.75 6h2.75Zm-.121-1.5L9.5 1.621V4.25c0 .138.112.25.25.25h2.629ZM2 1a1 1 0 0 1 1-1h6.586a1 1 0 0 1 .707.293l3.414 3.414a1 1 0 0 1 .293.707V15a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V1Zm3.75 7a.75.75 0 0 0 0 1.5h4.5a.75.75 0 0 0 0-1.5h-4.5ZM5 11.25a.75.75 0 0 1 .75-.75h2.5a.75.75 0 0 1 0 1.5h-2.5a.75.75 0 0 1-.75-.75Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $readme = "";
                    }
                    if ($projet['license_url']) {
                        $licence = "<a href='".$projet['license_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='".$projet['license']['name']."'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M8 10.5a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9ZM14 6a5.997 5.997 0 0 1-2.886 5.13l.58 3.185L12 16l-1.623-.544L8 14.66l-2.377.796L4 16l.306-1.684.58-3.187A6 6 0 1 1 14 6Zm-7.748 6h3.496l.322 1.772-1.594-.534-.476-.16-.476.16-1.594.534L6.252 12ZM9.5 6a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0ZM11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $licence = "";
                    } 
                    echo "
                    <div class='col'>
                        <div class='card h-100' style='background-color:#f1f3f5;'>
                            <div class='card-body p-2'>
                                <div style='float:left;padding-right:10px'>".$img."</div>
                                <a href='" . $projet['web_url'] . "' target='_blank'>" . $projet['name'] . "</a>
                            </div>
                            <div class='card-footer text-center ps-1 pe-1' style='border:none;padding-top:0;color:#6c757d;background-color:#f1f3f5;'>

                                <span data-bs-toggle='tooltip' style='cursor:help;' data-bs-html='true' data-bs-title='".htmlentities($description)."'><svg  class='description' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM4.927 4.99c-.285.429-.427.853-.427 1.27 0 .203.09.392.27.566.18.174.4.26.661.26.443 0 .744-.248.903-.746.168-.475.373-.835.616-1.08.243-.244.62-.366 1.134-.366.439 0 .797.12 1.075.363.277.242.416.54.416.892a.97.97 0 0 1-.136.502 1.91 1.91 0 0 1-.336.419 14.35 14.35 0 0 1-.648.558c-.34.282-.611.525-.812.73-.2.205-.362.443-.483.713-.322 1.245 1.35 1.345 1.736.456.047-.086.118-.18.213-.284.096-.103.223-.223.382-.36a41.14 41.14 0 0 0 1.194-1.034c.221-.204.412-.448.573-.73a1.95 1.95 0 0 0 .242-.984c0-.475-.141-.915-.424-1.32-.282-.406-.682-.726-1.2-.962-.518-.235-1.115-.353-1.792-.353-.728 0-1.365.14-1.911.423-.546.282-.961.637-1.246 1.066Zm2.14 7.08a1 1 0 1 0 2 0 1 1 0 0 0-2 0Z' fill='#6c757d'/></svg></span>

                                ".$readme."

                                ".$licence."

                                <svg class='ms-2' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M7.454 1.694a.591.591 0 0 1 1.092 0l1.585 3.81a.25.25 0 0 0 .21.154l4.114.33a.591.591 0 0 1 .338 1.038L11.658 9.71a.25.25 0 0 0-.08.247l.957 4.015a.591.591 0 0 1-.883.641l-3.522-2.15a.25.25 0 0 0-.26 0l-3.522 2.15a.591.591 0 0 1-.883-.641l.957-4.015a.25.25 0 0 0-.08-.247L1.207 7.026a.591.591 0 0 1 .338-1.038l4.113-.33a.25.25 0 0 0 .211-.153l1.585-3.81Z' fill='#6c757d'/></svg><span class='ms-1' style='font-size:80%'>".$projet['star_count']."</span>                                 

                            </div>
                        </div>
                    </div>";
                }
                ?>
                </div>
                <!-- /CARDS -->

            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="fw-bold font-monospace fs-3 lh-1">PROJETS LES PLUS BIFURQUÉS</div>
                <div class="fw-bold font-monospace text-muted small lh-1">en nombre de bifurcations ("forks")</div>


                <?php
                /*
                <table class="table table-borderless table-sm font-monospace small mt-2">
                <?php
                foreach ($projets_forked as $projet) {
                    echo "
                    <tr>

                        <td style='vertical-align: top;'>
                            ";
                            if ($projet['avatar_url']) {
                                echo "<img class='m-1' style='border-radius: 50%;' src='" . $projet['avatar_url'] . "' width='24' />";
                            } else {
                                echo "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='24' />";
                            } 
                            echo "
                        </td>

                        <td style='width:100%'>
                            <a href='" . $projet['web_url'] . "' target='_blank'>" . $projet['name'] . "</a>";
                            if ($projet['description']) echo "<div class='text-muted'>" . $projet['description'] . "</div>"; 
                            echo "
                        </td>

                        <td style='text-align:center;vertical-align:middle;'>
                            <svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M5.5 3.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm-.25 2.386a2.501 2.501 0 1 0-1.5 0v.364a2.5 2.5 0 0 0 2.5 2.5 1 1 0 0 1 1 1v.364a2.501 2.501 0 1 0 1.5 0V9.75a1 1 0 0 1 1-1 2.5 2.5 0 0 0 2.5-2.5v-.364a2.501 2.501 0 1 0-1.5 0v.364a1 1 0 0 1-1 1c-.681 0-1.3.273-1.75.715a2.492 2.492 0 0 0-1.75-.715 1 1 0 0 1-1-1v-.364ZM11.5 4.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2Zm-3.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z' fill='#000'/></svg><br />".$projet['forks_count']."
                        </td>

                        <td>";
                            if ($projet['readme_url']) echo "<div><a role='button' href='".$projet['readme_url']."' class='btn btn-secondary' style='--bs-btn-padding-y: .01rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .7rem;border-radius:3px;margin-bottom:1px;' target='_blank'>readme</a><div>";
                            if ($projet['license_url']) echo "<div><a role='button' href='".$projet['license_url']."' class='btn btn-secondary' style='--bs-btn-padding-y: .01rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .7rem;border-radius:3px;' target='_blank'>licence</a><div>";
                            echo "
                        </td>                        

                    </tr>";
                }
                ?>
                </table>
                */?>


                <!-- CARDS -->
                <div class="mt-2 row row-cols-1 row-cols-md-6 g-2 font-monospace small">
                <?php
                foreach ($projets_forked as $projet) {
                    if ($projet['avatar_url']) {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='" . $projet['avatar_url'] . "' width='32' />";
                    } else {
                        $img = "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='32' />";
                    } 
                    if ($projet['description']) {
                        $description = $projet['description'];
                    } else {
                        $description = "Pas de description...";
                    }
                    if ($projet['readme_url']) {
                        $readme = "<a href='".$projet['readme_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='README'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M12.5 6v8.5h-9v-13H8v2.75C8 5.216 8.784 6 9.75 6h2.75Zm-.121-1.5L9.5 1.621V4.25c0 .138.112.25.25.25h2.629ZM2 1a1 1 0 0 1 1-1h6.586a1 1 0 0 1 .707.293l3.414 3.414a1 1 0 0 1 .293.707V15a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V1Zm3.75 7a.75.75 0 0 0 0 1.5h4.5a.75.75 0 0 0 0-1.5h-4.5ZM5 11.25a.75.75 0 0 1 .75-.75h2.5a.75.75 0 0 1 0 1.5h-2.5a.75.75 0 0 1-.75-.75Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $readme = "";
                    }
                    if ($projet['license_url']) {
                        $licence = "<a href='".$projet['license_url']."' target='_blank' data-bs-toggle='tooltip' data-bs-html='true' data-bs-title='".$projet['license']['name']."'><svg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M8 10.5a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9ZM14 6a5.997 5.997 0 0 1-2.886 5.13l.58 3.185L12 16l-1.623-.544L8 14.66l-2.377.796L4 16l.306-1.684.58-3.187A6 6 0 1 1 14 6Zm-7.748 6h3.496l.322 1.772-1.594-.534-.476-.16-.476.16-1.594.534L6.252 12ZM9.5 6a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0ZM11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z' fill='#6c757d'/></svg></a>";
                    } else { 
                        $licence = "";
                    } 
                    echo "
                    <div class='col'>
                        <div class='card h-100' style='background-color:#f1f3f5;'>
                            <div class='card-body p-2'>
                                <div style='float:left;padding-right:10px'>".$img."</div>
                                <a href='" . $projet['web_url'] . "' target='_blank'>" . $projet['name'] . "</a>
                            </div>
                            <div class='card-footer text-center ps-1 pe-1' style='border:none;padding-top:0;color:#6c757d;background-color:#f1f3f5;'>

                                <span data-bs-toggle='tooltip' style='cursor:help;' data-bs-html='true' data-bs-title='".htmlentities($description)."'><svg  class='description' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM4.927 4.99c-.285.429-.427.853-.427 1.27 0 .203.09.392.27.566.18.174.4.26.661.26.443 0 .744-.248.903-.746.168-.475.373-.835.616-1.08.243-.244.62-.366 1.134-.366.439 0 .797.12 1.075.363.277.242.416.54.416.892a.97.97 0 0 1-.136.502 1.91 1.91 0 0 1-.336.419 14.35 14.35 0 0 1-.648.558c-.34.282-.611.525-.812.73-.2.205-.362.443-.483.713-.322 1.245 1.35 1.345 1.736.456.047-.086.118-.18.213-.284.096-.103.223-.223.382-.36a41.14 41.14 0 0 0 1.194-1.034c.221-.204.412-.448.573-.73a1.95 1.95 0 0 0 .242-.984c0-.475-.141-.915-.424-1.32-.282-.406-.682-.726-1.2-.962-.518-.235-1.115-.353-1.792-.353-.728 0-1.365.14-1.911.423-.546.282-.961.637-1.246 1.066Zm2.14 7.08a1 1 0 1 0 2 0 1 1 0 0 0-2 0Z' fill='#6c757d'/></svg></span>

                                ".$readme."

                                ".$licence."

                                <svg class='ms-2' width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M5.5 3.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm-.25 2.386a2.501 2.501 0 1 0-1.5 0v.364a2.5 2.5 0 0 0 2.5 2.5 1 1 0 0 1 1 1v.364a2.501 2.501 0 1 0 1.5 0V9.75a1 1 0 0 1 1-1 2.5 2.5 0 0 0 2.5-2.5v-.364a2.501 2.501 0 1 0-1.5 0v.364a1 1 0 0 1-1 1c-.681 0-1.3.273-1.75.715a2.492 2.492 0 0 0-1.75-.715 1 1 0 0 1-1-1v-.364ZM11.5 4.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2Zm-3.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z' fill='#6c757d'/></svg><span class='ms-1' style='font-size:80%'>".$projet['forks_count']."</span>                                 

                            </div>
                        </div>
                    </div>";
                }
                ?>
                </div>
                <!-- /CARDS -->                

            </div>
        </div>
    </div>

    <br />
    <br />
    <br />
    <br />

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <script>
        // Initialize all tooltips
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })
    </script>

  </body>
</html>
